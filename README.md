# Packer-script-images

The limited time avaliable for professionals and academical specialists is too short to check every possible configuration created by them or their students. 
With the help of an automated toolchain it is possible to simplify and quicken the configuration of a virtual network.
An automated virtual network gives the user the opportunity to simulate a real network in the personal device, without delay productivity and a lot of money and/or time.

## Purpose
The purpose of the present project is to provid a reliable auto-generated vSRX firewall with specific configurations using Packer hashicorp.


![diagram](https://gitlab.com/Mikepamp/packer-script-images/raw/master/Packer_router_diagram.png)





## Toolchain 

When GitLab repository has been cloned and Git repository has all the necessary files, Packer JSON file is ready to run and create the new virtual router in collaboration with the builder (Vmware workstation). 
Run the command ”packer build FILE_NAME” to start the new VM. Packer starts building the new VM based on the source image that has been converted from ovf to vmx, and then start booting the Junos CLI commands into the new virtual router. 
During the booting process, Junos command ”load override {{HTTPIP}} PATH_TO_JUNIPER_FILE” allow Packer to connect to the new virtual router, and change the factory default configuration to the new configuration that has been pulled from the Git repository. 
The last thing packer does, after shutdown the new virtual router is to copy the new virtual router in a specific directory into FreeNAS file server

## Steps

1. download/get vsrx ovf file
2. put it in ./junos-vsrx-ovf (or similar) directory
3. create source dir: mkdir junos-vsrx-in
4. convert to vmx: ovftool junos-vsrx-ovf/junos-xxx.ovf junos-vsrx-in/junos-vsrx.vmx
5. packer build packer-router.json
6. Store the juniper coniguration file in Git and push it to the vm

