#!/usr/bin/env bash

# fail script if anything fails
set -e

OVFFILE=junos-vsrx-12.1X47-D15.4-domestic.ovf
VMXFILE=junos-vsrx.vmx

BASEDIR=$(dirname $0)
cd $BASEDIR

# Download ovf file from juniper.net
curl -k -O https://10.217.19.225:8081/WebShare/$OVFFILE
cp $OVFFILE $HOME/git/packer-script-images/$VMXFILE

#converted to vmx
if [ ! -e junos-vsrx-in/$VMXFILE ]; then
	ovftool -tt=vmx junos-vsrx/$OVFFILE junos-vsrx-in/$VMXFILE
else
	echo file exists
fi

