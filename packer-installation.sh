#!/usr/bin/env bash

# fail script if anything fails
set -e

BASEDIR=$(dirname $0)
cd $BASEDIR

echo " ** update, and install packer"
sudo apt-get update -y
sudo apt-get install packer -y
#sudo wget https://releases.hashicorp.com/packer/1.3.5/packer_1.3.5_linux_amd64.zip
#sudo unzip packer_1.3.5_linux_amd64.zip -d /usr/local/packer
cd ~/
export PATH="$PATH:/usr/local/packer"

if [ -f /usr/local/packer/packer ]; then
	sudo ln -s /usr/local/packer/packer packer
fi

packer version

